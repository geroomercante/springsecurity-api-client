package gmercante.springsecurity.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.stereotype.Component;

import gmercante.springsecurity.util.Permission;

@Configuration
@Component
@EnableWebSecurity
public class HttpSecurityConfig {

  @Autowired
  private AuthenticationProvider authenticationProvider;
  
  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

    http
      .csrf( csrfConfig -> csrfConfig.disable() ) // Vamos a estar trabajando con tokens, esta vulnerabilidad nos da igual.
      .sessionManagement( sessionMangConfig -> sessionMangConfig.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
      .authenticationProvider(authenticationProvider)
      .authorizeHttpRequests( authConfig -> {
        // Public Endpoints
        authConfig.requestMatchers(HttpMethod.POST, "/auth/login").permitAll();
        authConfig.requestMatchers(HttpMethod.GET, "/auth/public-access").permitAll();
        authConfig.requestMatchers("/error").permitAll();
        // Protected Endpoints
        authConfig.requestMatchers(HttpMethod.GET, "/products").hasAuthority(Permission.READ_ALL_PRODUCTS.name());
        authConfig.requestMatchers(HttpMethod.POST, "/products").hasAuthority(Permission.SAVE_ONE_PRODUCT.name());
        // Any Endpoints. -> Cualquier endpint que se me haya olvidado declarar, va a ser denegado el acceso.
        authConfig.anyRequest().denyAll();
      });

    return http.build();
  }
}
