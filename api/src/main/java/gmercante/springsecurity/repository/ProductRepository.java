package gmercante.springsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gmercante.springsecurity.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> { }
