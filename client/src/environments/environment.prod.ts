export const environment = {
  production: true,
  env: 'PROD',
  api: 'http://localhost:8080/api/v1'
};
