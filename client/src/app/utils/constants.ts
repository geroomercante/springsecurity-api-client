export type Null<T> = T | undefined | null;

export const TOAST_DURATION = 4000;
export const MAX_PAGE_SIZE = 100000;
export const MAP_ICON_SIZE = 28;

export const enum ToastMessageTypeEnum {
    DEFAULT,
    SUCCESS,
    INFO,
    WARNING,
    ERROR,
};

export enum Confirm {
    ACCEPT = "ACCEPT",
    CANCEL = "CANCEL",
    CLOSE = "CLOSE"
}

export enum ThemeColor {
    BG_APP = 'BG_APP',
    PRIMARY = 'PRIMARY',
    ON_PRIMARY = 'ON_PRIMARY',
    ACCENT = 'ACCENT',
    ON_ACCENT = 'ON_ACCENT',
    WARN = 'WARN',
    ON_WARN = 'ON_WARN',
    DISABLED = 'DISABLED',
    ON_DISABLED = 'ON_DISABLED'
}
