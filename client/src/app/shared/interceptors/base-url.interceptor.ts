import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        // Ajusta la URL de la solicitud con el baseUrl
        const apiRequest = request.clone({ url: `${environment.api}/${request.url}` });

        // Continúa con la solicitud ajustada
        return next.handle(apiRequest);
    }
}