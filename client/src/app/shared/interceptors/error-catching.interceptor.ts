import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpStatusCode } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { NavigationService } from '../../services/navigation.service';
import { IError } from 'src/app/models/interface/error.interface';

@Injectable()
export class ErrorCatchingInterceptor implements HttpInterceptor {

    constructor(
        private navigationService: NavigationService
    ) { }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        return next.handle(request).pipe(
            map(res => {
                return res
            }),
            catchError((error: HttpErrorResponse) => {
                return throwError(this.catchResponse(error));
            })
        );
    }

    private catchResponse(error: HttpErrorResponse): any {
        switch (error.status) {
            case HttpStatusCode.Unauthorized:
                this.logoutUser();
                break;
            default:
                if (error.error) {
                    try {
                        return error.error as IError;
                    } catch (err) {
                        return error.error;
                    }
                } else {
                    return {
                        Code: error.status.toString(),
                        Message: error.message
                    };
                }
        }
    }

    private logoutUser(): void {
        this.navigationService.toLogin();
    }
}