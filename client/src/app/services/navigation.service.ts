import { Injectable } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Null } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(
    public _router: Router,
    private _location: Location,
    private _activatedRoute: ActivatedRoute
  ) { }

  public navigateTo(url: string, options?: NavigationExtras): void {
    this._router.navigate([url], options);
  }

  public goBack(): void {
    this._location.back();
  }

  public toRegister(options?: NavigationExtras): void {
    this.navigateTo('/register', options);
  }

  public toLogin(options?: NavigationExtras): void {
    this.navigateTo('/login', options);
  }

  public toProfile(options?: NavigationExtras): void {
    this.navigateTo('/profile', options);
  }

  public toHome(options?: NavigationExtras): void {
    this.navigateTo('/home', options);
  }

  public toMenu(options?: NavigationExtras): void {
    this.navigateTo('/menu', options);
  }

  getCurrentPath(): Null<string> {
    return this._location.path().split('?').shift();
  }

  getCurrentPathToken(): Null<string> {
    return this._location.path().split('/').pop();
  }

}
