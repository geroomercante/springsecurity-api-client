import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Null } from '../utils/constants';
import { Session } from '../models/session.model';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    private readonly BASE_KEY: string = `CLIENT_${environment.env}_`;
    private readonly SESSION: string = "SESSION";

    private saveValue(key: string, value: any) {
        if (!value) return;
        localStorage.setItem(key, JSON.stringify(value));
    }

    private getValue<T>(key: string): Null<T> {
        let item = localStorage.getItem(key);
        if (!item) return null;
        try {
            return JSON.parse(item) as T;
        } catch (error) {
            return null;
        }
    }

    public saveSession(session: Session): void {
        this.saveValue(this.BASE_KEY + this.SESSION, session);
    }

    public getSession(): Null<Session> {
        return this.getValue<Session>(this.BASE_KEY + this.SESSION);
    }

    public clearSession(): void {
        localStorage.removeItem(this.BASE_KEY + this.SESSION);
    }

}
