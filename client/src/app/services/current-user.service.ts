import { Injectable } from '@angular/core';
import { Null } from '../utils/constants';
import { Session } from '../models/session.model';
import { LocalStorageService } from './local-storage.service';

@Injectable({
    providedIn: 'root'
})
export class CurrentUserService {

    constructor(private lsService: LocalStorageService) { }

    getSession(): Null<Session> {
        return this.lsService.getSession();
    }

    getToken(): Null<string> {
        return this.getSession()?.token;
    }

    isLoggedIn(): boolean {
        return !!this.getToken();
    }

}
