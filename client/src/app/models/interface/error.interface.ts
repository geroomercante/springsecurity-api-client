export interface IError {
    Code: string;
    Message: string;
    ExceptionMessage: string;
}