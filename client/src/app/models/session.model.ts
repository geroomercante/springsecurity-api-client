export class Session {
  id?: number;
  status?: string;
  email?: string;
  password?: string;
  userId?: string;
  profile?: string;
  startDate?: Date;
  endDate?: Date;
  token?: string;
  changePasswordToken?: string;
  experience?: number;
}