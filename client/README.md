# API - foco a `Spring Security 6`

## Filter Chain - Cadena de filtro `de Jakarta`
- En este momento no se ha vinculado el ciclo de vida del contenedor de Servlets de jakarta con el ciclo de vida 
de una aplicación Spring. La arquitectura que presenta de momentos es, un cliente, como Angular, react, otra API, etc.
haciendole peticiones a nuestro Servlet, que pasa por una cadena de filtros, filtro 1, f2, f2, fn, en una peticion se antepone 
toda la cadena de filtros, los cuales dictaminan si se tiene acceso o no a este servlet. Luego tenemos el componente DelegatingFilterProxy 

## DelegatingFilterProxy
- Es un filtro, que se inyecta en la cadena de filtros de `Spring Security` y lo que permite es crear un puente entre el ciclo de vida del contenedor
de Servlet de jakarta y el ApplicationContext de Spring. Cuanto el DelegatingFilterProxy ha terminado, lo que hace es delegar el "trabajo" a este otro `Bean`
ese otro Bean, se llama FilterChainProxy.

## FilterChainProxy
- Es un proxy o una "antesala" para la cadena de filtros "FilterChain", pero ya propia de `Spring Security`, es un FilterChain propio de Jakarta, el que siempre está.
Se inyecta este Filtro "DelegatingFilterProxy" el cual hace la vinculación con el ciclo de vida de ambas apps, le delega el trabajo a este proxy "FilterChainProxy"
el cual coordina la ejecución de otra FilterChain que es la de SecurityFilterChain, o cadena de filtros de seguridad de Spring.

## SecurityFilterChain
- FilterChainProxy lo utiliza para dictaminar que instancia de filtros deben invocarse para la solicitud actual. Puede haber varias instancias de SecurityFilterChain, para 
distintos endpoints o rutas de nuestra aplicación.

* [Spring Security Servlet - Arquitectura](https://docs.spring.io/spring-security/reference/servlet/architecture.html)